#include <fcntl.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

/* CORE API */
/*
 * PCG construction
 * seeding the RNG means merely setting the initial state.
 * the increment could also be made part of the seed, just make sure it's odd.
 */
uint32_t
rng32_r(uint64_t *state, uint64_t id)
{
	uint64_t oldstate = *state;
	uint32_t r, v;

	*state *= 6364136223846793005ULL;
	*state += (id << 1) | 1; /* we must have it as odd */

	r = oldstate >> (64 - 5);
	v = (oldstate ^ (oldstate >> 18)) >> (32 - 5);
	v = (v >> (-r & 31)) | (v << r);
	return v;
}

/*
 * Based on optimized Lemire's method
 * https://pcg-random.org/posts/bounded-rands.html
 */
uint32_t
rng32_bounded_r(uint64_t *state, uint64_t id, uint32_t bound) {
	uint32_t x = rng32_r(state, id);
	uint64_t m = (uint64_t)x * (uint64_t)bound;
	uint32_t l = (uint32_t)m;
	if (l < bound) {
		uint32_t t = -bound;
		if (t >= bound) {
			t -= bound;
			if (t >= bound) 
				t %= bound;
		}
		while (l < t) {
			x = rng32_r(state, id);
			m = (uint64_t)x * (uint64_t)bound;
			l = (uint32_t)m;
		}
	}
	return m >> 32;
}

/* Initialize state with somewhat random number */
int
entropy_buf(void *dst, size_t size)
{
	int fd = open("/dev/urandom", O_RDONLY);
	if (fd < 0)
		return -1;
	ssize_t sz = read(fd, dst, size);
	return (close(fd) == 0) ? sz : -1;
}

int
rng32_buf_r(uint64_t *state, uint64_t id, char *dst, size_t size)
{
	size_t i, j, r;
	for (i = 0; i < size/4; i++) {
		r = rng32_r(state, id);
		for (j = 0; j < 4; j++) {
			dst[i*4+j] = r&0xff;
			r >>= 8;
		}
	}
	if (size % 4) {
		r = rng32_r(state, id);
		for (i = size / 4; i < size; i++) {
			dst[i] = r&0xff;
			r >>= 8;
		}
	}
	return size;
}

void
rng32_randomseed_r(uint64_t *state)
{
	if (entropy_buf(state, sizeof(*state)) < 0) {
		struct timespec ts;
		clock_gettime(CLOCK_REALTIME, &ts);
		*state = (intptr_t)&printf ^ ts.tv_sec ^ ((unsigned long)ts.tv_nsec * 0xAC5533CD);
	}
}

/* GLOBAL FUNCTIONS */
static uint64_t globalstate;
static uint64_t globalid;

uint32_t
rng32(void)
{
	return rng32_r(&globalstate, globalid);
}

int
rng32_buf(char *dst, size_t size)
{
	return rng32_buf_r(&globalstate, globalid, dst, size);
}

void
rng32_seed(uint64_t state, uint64_t id)
{
	globalstate = state;
	globalid = id;
}

uint64_t
rng32_bounded(uint32_t bound)
{
	return rng32_bounded_r(&globalstate, globalid, bound);
}

void
rng32_randomseed(void)
{
	rng32_randomseed_r(&globalstate);
	globalid = 1442695040888963407ULL;
}

/* UTILITIES */
double
rng32_double_r(uint64_t *state, uint64_t id)
{
	return ldexp(rng32_r(state, id), -32);
}

double
rng32_double(void)
{
	return rng32_double_r(&globalstate, globalid);
}

/* DISTRIBUTIONS, GLOBAL */
double
normal_distrib(void)
{
	static double z0, z1;
	static int cached = 0;
	if (cached) {
		cached = 0;
		return z1;
	}
	double u1, u2, r, theta;

	do
		u1 = rng32_double();
	while (u1 == 0.0);

	u2 = rng32_double();
	r = sqrt(-2.0 * log(u1));
	theta = 2*M_PI*u2;

	z0 = r*cos(theta);
	z1 = r*sin(theta);
	cached = 1;
	return z0;
}

int
bernoulli_distrib(float p)
{
	return rng32() < (uint32_t)-1 * p;
}
