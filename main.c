#include <string.h>
#include <stdio.h>
// #include <sys/tree.h>
#include "tree.h"
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include "arg.h"

#ifndef N
#define N 5
#endif

struct node
{
	RB_ENTRY(node) entry;
	uint64_t array[256]; /* stores char */
	char key[N];
	uint64_t i;
};

int nodecmp(struct node *x, struct node *y){
	return memcmp(x->key, y->key, N);
}

RB_HEAD(nodetree, node) head = RB_INITIALIZER(&head);
RB_PROTOTYPE(nodetree, node, entry, nodecmp)
RB_GENERATE(nodetree, node, entry, nodecmp)

void
usage()
{
	fprintf(stderr, "%s: [-p] [-b begin]\n", argv0);
	exit(1);
}

uint32_t rng32_bounded(uint32_t);
uint32_t rng32_randomseed(void);

int
main(int argc, char *argv[])
{
	char *buf, *begin;
	int c;
	struct node find, *res;
	int l, len, pflag;
	unsigned char u;
	int64_t r, inc;

	pflag = 0;
	begin = "";
	ARGBEGIN{
	case 'b':
		begin = EARGF(usage());
		break;
	case 'p':
		pflag = 1;
		break;
	default:
		usage();
	}ARGEND;

	len = strlen(begin);
	char word[2000];
	buf = find.key;

	memset(buf, 0, N);
	if(pflag)
		fscanf(stdin, "%ld\t", &inc);
	else
		inc = 1;

	while((c = fgetc(stdin)) != EOF){
		res = RB_FIND(nodetree, &head, &find);
		if(res == NULL){
			res = malloc(sizeof(struct node));
			memcpy(res->key, buf, N);
			res->i = 0;
			memset(res->array, 0, sizeof res->array);
			RB_INSERT(nodetree, &head, res);
		}
		res->i += inc;
		res->array[(unsigned char)c] += inc;

		memmove(buf, buf+1, N-1);
		buf[N-1] = (char)c;
		if(c == '\n'){
			memset(buf, 0, N);
			if(pflag)
				fscanf(stdin, "%ld\t", &inc);
		}

	}

	rng32_randomseed();
	for(int j = 0; j < 100; j++){
		memset(buf, 0, N);
		memset(word, 0, sizeof word);
		strcpy(word, begin);
		l = len;
		memmove(buf+N-MIN(l,N), word+MAX(0,l-N), MIN(N, l)); /* copy word at the beginning of buf */
		c = '\0';
		while(c != '\n'){
			res = RB_FIND(nodetree, &head, &find);
			if(res == NULL) break;
			r = rng32_bounded(res->i);
			for(u = 0; r >= 0; r -= res->array[u++]);
			c = (char)u-1;

			memmove(buf, buf+1, N-1);
			buf[N-1] = c;
			word[l++] = c;
		}
		word[l++] = '\0';
		printf("%s", word);
	}
}
