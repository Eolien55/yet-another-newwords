Warning: some info is outdated:

- config.h doesn't exist
- there is no makefile (`cc random.c main.c` is usually enough)
- things are done using flags

Probably requires something like bsd-compat-headers for sys/tree.h.
tree.h is provided as a replacement.

Overall: believe the source code, not the README.

Note: random.c is more complex than needed, but I really like this small
collection of random functions (along with a PRNG implementation!). The
seed isn't truly random, but probably quite sufficiently.

# NewrWords

This simple program generates random words, that could be real words,
written in C.

I used really simple algorithms to achieve this, so the result isn't
totally perfect.

This is way more primitive than GPT, or other very complex generative models.
But hey, I like fooling around

## Motivation

I was **HEAVILY** inspired by [ScienceEtonnante's own algorithm, and
video](https://www.youtube.com/watch?v=YsR7r2378j0) (link for french
folks).  Unfortunately, the code was pretty ugly (unnamed variables, like
i, j, etc., hard-coded values, inconstitencies in code...), and really
slow. The code used to parse the dictionary was indeed written in Python
(though the code used to generate the words heavily relied on NumPy,
which is known to be extremely fast). Some parts of the code were really
slow due to being basically trial and error (O(∞) if you're unlucky),
and since the algorithm was fairly simple, I decided to rewrite it.

We lost some features though. ScienceEtonnante's algorithm could draw
graphs to represent the dictionary, while C cannot do this just by
itself. I think I can write a simple GnuPlot script to do this, though.

## How it works

The algorithm has the N last characters stored in memory, and it tries
to predict the N+1th. Then, it uses the rotates the list, making the Nth
character the N-1th and tries to predict the new Nth character. This is
basic Markov chains.

So, the word is generated until the last character represents the end
of a word, aka a newline character.

Then, we output the new generated word (with a few rules, like the number
of words of a certain length, or the maximum and minimum sizes. These
are configured by editing the source code ([see below](#options))). And
we repeat the process, until we generated the right amount of words,
of the right sizes.

## Warning

In english, the result is very poor. I suppose that it is because
english words are very different, and because the spelling "rules"
aren't as strict as in other languages (like french, or swedish).

The french result, however, is fairly good in my opinion. Swedish and
spanish look like words that could actually exist.

## How to use

To use you must have : a copy of this program, and a dictionary ([see
below if you don't have one](#libreoffice-dictionaries)).

To get this program, do :

`git clone https://codeberg.org/Eolien55/NewWords`

`cd NewWords`

You should also have a C99-compatible C compiler and make (optional)
installed.

Compile using `make`, then run `newwords <dictionary.dic` to get a list
of fresh new words!

### LibreOffice dictionaries

To obtain a dictionary, you can do the following :

Search for a dictionary at
<https://cgit.freedesktop.org/libreoffice/dictionaries/tree>. The
dictionary you get might not be perfect, so you can change it to your
needs. What I recommend : the words use this scheme `<file>/<type>`. So,
you can run something like `sed -i 's#^.*/<type>$##' <dictionary.dic>`,
followed by a `sed -i '/^$/d' <dictionary.dic>` to remove empty lines.

What I personaly use :

```bash
for pat in <pattern-list>; do
	sed -i "s#^.*/$pat\$##" <dictionary.dic>
done
 
sed -i '/^$/d' <dictionary.dic>
  
sed -i 's#/.*$##' <dictionary.dic>
```

Just replace `<pattern-list>` by a list of patterns seperated by commas
and enclosed by quotes, and `<dictionary.dic>` by the actual file you
are treating.

What this does :
* get a list of type of words to remove
* delete all the lines that are of this type
* repeat until the list is empty
* delete all empty lines
* and, finally, delete the second part of the word, aka the `/<type>`.

## Options

Many options are configurable via `config.h` : the "n" in "n-grams"
(this can be seen as a precision value ; the more it is high the more
it will look like the target language ; but the more it is high, the
less likely it is to create new words), the number of words to generate
(`K`, for some obscure reason), the minimum and maximum word lengths
(`LEN_MIN` and `LEN_MAX` with values 2 and 20 by default, respectively ;
these default values are meant to be changed for experimentation), and
`PONDERATE` option ([see below](#ponderate)).

### Ponderate

Some results in some languages are _awful_. For example, English looks
nothing like English. This is because of the fact that for this program,
each word is just as frequent (which is false). To fix this problem, one
may use "ponderated dictionaries", such as the ones provided by [Google's
Ngram](https://storage.googleapis.com/books/ngrams/books/datasetsv3.html).

Ponderated dictionaries are of the following format :
`587<tab>example`. The program will segfault if the format is not strictly
followed (remember, one number, one tab, one word per line).

## Licensing

This is free software, and licensed under the AGPL-3 license.
